const HtmlWebPackPlugin = require("html-webpack-plugin");
const path = require("path");

// TODO: code splitting and hashing

module.exports = {
  entry: ["./src/index.js"],
  output: {
    path: path.resolve(__dirname, "./build"),
    filename: "bundle.js",
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      }
    ]
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: "./public/index.html"
    })
  ],
  devServer: {
    port: 8080
  }
};