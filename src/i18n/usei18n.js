import { useContext } from "react";
import Polyglot from "node-polyglot";

import LanguageContext from "./LanguageContext";
import { dict } from "../dict";

const usei18n = () => {
  const currentLang = useContext(LanguageContext);
  const polyglot = new Polyglot();
  polyglot.extend(dict[currentLang]);

  const i18n = key => polyglot.t(key);

  return { currentLang, i18n };
};

export default usei18n;
