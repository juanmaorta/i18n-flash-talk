const LANG_ENGLISH = "en";
const LANG_FRENCH = "fr";
const LANG_SPANISH = "es";
const LANG_CATALAN = "ca";

export const languages = [
  LANG_ENGLISH,
  LANG_SPANISH,

  LANG_CATALAN,
  // LANG_FRENCH,
];
