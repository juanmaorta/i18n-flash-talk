export const en = {
  'flash_talk': 'Flash Talk',
  'using_context': 'Using Context',
  'and': '&',
  'to_i18n_your_app': 'to i18n your App',
  'features': 'features',
  'how_to_use_context': 'How to use the LanguageContext',
  'context': 'we use Context to pass the language selected down the node tree',
  'translation': 'translating function - polyglot',
  'hook': 'i18n hook (should use the language context function)',
  'the_hook_in_use': 'how to use the hook',
  'current_lang': 'current language',
};
