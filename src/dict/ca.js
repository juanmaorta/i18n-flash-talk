export const ca = {
  'features': 'característiques',
  'translation': 'funció de traducció - polyglot',
  'current_lang': 'has triat',
  'flash_talk': 'Flash Talk',
  'using_context': 'Usant Context',
  'and': 'i',
  'to_i18n_your_app': 'per i18n la teva App',
  'how_to_use_context': 'Com fer servir LanguageContext',
  'context': 'Fem servir Context per passar l\'idioma seleccionat per l\'arbre de nodes',
  'hook': 'i18n hook (fa servir la funció de traducció)',
  'the_hook_in_use': 'el hook en ús',
};
