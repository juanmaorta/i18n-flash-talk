export const fr = {
  'traits': 'features',
  'context': 'context - passer la langue sélectionnée dans l\'arbre',
  'translation': 'fonction de traduction - polyglot',
  'hook': 'i18n hook (should use the language context function)',
  'current_lang': 'language choisi',
};
