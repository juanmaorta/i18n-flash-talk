export const es = {
  'features': 'características',
  'translation': 'función de traducción - polyglot',
  'hook': 'un hook para i18n (usa la función de traducción)',
  'current_lang': 'el idioma elegido es',

  'flash_talk': 'Flash Talk',
  'using_context': 'Usando Context',
  'and': 'y',
  'to_i18n_your_app': 'para i18n tu App',
  'how_to_use_context': 'Cómo usar LanguageContext',
  'context': 'usamos Context para pasar el idioma seleccionado por el árbol de nodos',
  'the_hook_in_use': 'Cómo usar el hook',
};
