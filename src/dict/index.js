import { en } from "./en";
import { es } from "./es";
import { fr } from "./fr";
import { ca } from "./ca";

export const dict = {
  en,
  es,
  // fr,
  ca,
};
