import React, { useState } from "react";
import LanguageSelector from "./components/LanguageSelector";
import Contents from "./components/Contents";
import { languages } from "./constants";
import { LanguageContext } from "./i18n";

const App = () => {
  console.log(window);
  const defaultLang = languages[0];
  const [lang, setLang] = useState(defaultLang);

  return (
    <LanguageContext.Provider value={lang}>
      <Contents
       current={ lang }
       setLanguage={setLang}
      />
    </LanguageContext.Provider>
  );
}

export default App;
