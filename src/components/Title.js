import React from "react";
import styled from "styled-components";

export const Title = styled.h1`
  font-size: 18px;
  font-weight: 300;
`;