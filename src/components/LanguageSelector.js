import React from "react";
import styled from "styled-components";

import { usei18n } from "../i18n";
import { languages } from "../constants";

import { Title } from "./Title";

const LanguageList = styled.div`
  display: flex;
  flex-direction: row;
  /* width: 25%; */
`;

const LangButton = styled.button`
  background-color: #0000CC;
  color: white;
  border: none;
  border-radius: 4px;
  padding: 8px 16px;
  font-size: 10px !important;
  cursor: pointer;
  margin-right: 8px;
  flex: 1;

  &:disabled {
    background-color: #ddd;
    color: #333;
    cursor: default;
  }
`;

const LanguageSelector = ({ setLanguage }) => {
  const { currentLang, i18n } = usei18n();

  return (
    <div style={{ marginTop: -150, marginBottom: 16, paddingBottom: 6, borderBottom: "solid 1px #000" }}>
      <Title>{i18n('current_lang')}: <strong>{ currentLang }</strong></Title>
      <LanguageList>
        {
          languages.map( (lang, idx) => {
            return (
              <LangButton
                key={idx}
                disabled={ lang === currentLang }
                onClick={() => setLanguage(lang)}>
                  {lang}
              </LangButton>
            )
          })
        }
      </LanguageList>
    </div>
  );
}

export default LanguageSelector;
