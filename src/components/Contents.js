import React from "react";
import { Deck, Slide, Text, CodePane, Appear } from 'spectacle';
import createTheme from 'spectacle/lib/themes/default';

import TheContext from "!!raw-loader!../i18n/LanguageContext.js";
import TheHook from "!!raw-loader!../i18n/usei18n.js";
import TheUse from "!!raw-loader!./app.example";
import HookExample from "!!raw-loader!./hook_example.example";

import { usei18n } from "../i18n";
import LanguageSelector from "./LanguageSelector";

const theme = createTheme(
  {
    primary: 'grey',
    secondary: 'white'
  },
  {
    primary: 'Roboto',
    secondary: {
      name: 'Droid Serif',
      googleFont: true,
      styles: ['400', '700i']
    }
  }
);

const Contents = ({ lang, setLanguage }) => {
  const { i18n } = usei18n();

  return (
    <Deck theme={ theme } height="100">
      <Slide>
        <LanguageSelector
          current={ lang }
          setLanguage={ setLanguage }
        />
        <Text style={{ fontWeight: "bold", marginBottom: 40, fontSize: 80 }}>{ i18n('flash_talk') }</Text>
        <Text>{ i18n('using_context') }</Text>
        <Text>{ i18n('and') }</Text>
        <Text>{ i18n('hooks') }</Text>
        <Text>{ i18n('to_i18n_your_app') }</Text>
      </Slide>
      <Slide>
        <Text>{ i18n('context') }</Text>
        <Appear>
          <div>
            <CodePane source={TheContext} lang="javascript" />
          </div>
        </Appear>
      </Slide>

      <Slide>
        <Text>{ i18n('how_to_use_context') }</Text>
        <Appear>
          <div>
            <CodePane source={TheUse} lang="javascript" />
          </div>
        </Appear>
      </Slide>

      <Slide>
        <Text>{ i18n('hook') }</Text>
        <Appear>
          <div>
            <CodePane source={TheHook} lang="javascript" />
          </div>
        </Appear>
      </Slide>

      <Slide>
        <Text>{ i18n('the_hook_in_use') }</Text>
        <Appear>
          <div>
            <CodePane source={HookExample} lang="javascript" />
          </div>
        </Appear>
      </Slide>

    </Deck>
  );
}

export default Contents;
